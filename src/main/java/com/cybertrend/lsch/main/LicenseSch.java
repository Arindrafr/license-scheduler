package com.cybertrend.lsch.main;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.spec.KeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cybertrend.license.activator.Activator;
import com.cybertrend.license.activator.FirstLayout;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.swing.*;



@Component
public class LicenseSch {

	private static String defaultPath = System.getProperty("user.home");
	private static String jsonKey = defaultPath + "/cbikey.decrypt";
	private static String chiper = "cb1pr0duk";
	private static String dechiper="ilovemonday";
	
	private JFrame frame;
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	

	@Scheduled(initialDelay=5000, fixedRate=50000)
	public void licenseSch() {

		logger.debug("Initial logger");
		FirstLayout window = new FirstLayout();
		Activator window2 =new Activator();
		File f = new File(jsonKey);
		if(f.exists() && !f.isDirectory()) { 
			window.frame.setVisible(true);
		}else {
			window2.frame.setVisible(true) 	;
		}
//		JFrame f=new JFrame("Meong"); 
//		f.setTitle("Meong");
//		f.setBounds(100, 100, 684, 495);
//		JButton b=new JButton("Meong");  
//		b.setBounds(130,100,100, 40);  
//		f.add(b);  
//		f.setSize(300,400);  
//		f.setLayout(null);  
//		f.setVisible(true);  
//		f.setTitle("Meong");
////		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
//		frame = new JFrame();
//		frame.setBounds(100, 100, 684, 495);
//		
//		frame.getContentPane().setLayout(null);
//		
//		JLabel lblLicenseActivation = new JLabel("License Activation");
//		lblLicenseActivation.setHorizontalAlignment(SwingConstants.CENTER);
//		lblLicenseActivation.setFont(new Font("Tahoma", Font.PLAIN, 18));
//		lblLicenseActivation.setBounds(252, 13, 145, 53);
//		frame.getContentPane().add(lblLicenseActivation);
//		
//		JLabel lblActivationDate = new JLabel("License Start");
//		lblActivationDate.setFont(new Font("Tahoma", Font.PLAIN, 17));
//		lblActivationDate.setBounds(93, 136, 107, 16);
//		frame.getContentPane().add(lblActivationDate);
//		
//		JLabel lblLicenseEnd = new JLabel("License End");
//		lblLicenseEnd.setFont(new Font("Tahoma", Font.PLAIN, 17));
//		lblLicenseEnd.setBounds(436, 136, 107, 16);
//		frame.getContentPane().add(lblLicenseEnd);
//		
//		JButton btnDeactivate = new JButton("DeActivate");
//		btnDeactivate.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent arg0) {
//				int response = JOptionPane.showConfirmDialog(null, "Are you sure?", "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//				if(response==JOptionPane.YES_OPTION) {
//					File file = new File(jsonKey);
//					file.delete();
//					frame.dispose();
//
//					
//				}
//			}
//		});
//		btnDeactivate.setBounds(446, 363, 97, 25);
//		frame.getContentPane().add(btnDeactivate);
//		
////		JLabel startDate = new JLabel(stDate);
////		startDate.setFont(new Font("Tahoma", Font.PLAIN, 15));
////		startDate.setHorizontalAlignment(SwingConstants.CENTER);
////		startDate.setBounds(49, 185, 203, 74);
////		frame.getContentPane().add(startDate);
////		
////		JLabel endDate = new JLabel(edDate);
////		endDate.setFont(new Font("Tahoma", Font.PLAIN, 15));
////		endDate.setHorizontalAlignment(SwingConstants.CENTER);
////		endDate.setBounds(388, 185, 195, 74);
////		frame.getContentPane().add(endDate);
////		logger.info("");
//		frame.setVisible(true);
		System.out.println(result());
//		 try
//	        { 
//	            // Command to create an external process 
//	            String command = "D:\\Dokumen\\Work\\CBI\\ActivatorLicense\\act.exe"; 
//	  
//	            // Running the above command 
//	            Runtime run  = Runtime.getRuntime(); 
//	            Process proc = run.exec(command); 
//	        } 
//	  
//	        catch (IOException e) 
//	        { 
//	            e.printStackTrace(); 
//	        } 
		
//		frame = new JFrame();
//		frame.setBounds(100, 100, 684, 495);
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.getContentPane().setLayout(null);
		
		
	}
	public String result() {
		File f = new File(jsonKey);
		if(f.exists() && !f.isDirectory()) { 

			File inputFile = new File(jsonKey);
			String jsonLine = secretWord(inputFile);
			
			JsonElement jelement = new JsonParser().parse(jsonLine);
			JsonObject jobject = jelement.getAsJsonObject();
			String stDate = jobject.get("activateTime").getAsString();
			String edDate = jobject.get("expiredTime").getAsString();
			
			stDate = convertDate(stDate);
			edDate = convertDate(edDate);
			
			if(compareDate(edDate)==1) {
				logger.info("Your License Active");
				return "Your License Active";
			}else {
				logger.info("Your License Expired");
				return "Your License Expired";
//				return new File(LicenseSch.class.getProtectionDomain().getCodeSource().getLocation()
//					    .toURI()).getPath();
			}
		}else {
			logger.info("You haven't activated license or License code missing");
			return "You haven't activated license or License code missing";
		}
	}
	
	public String secretWord(File inputFile) {
		try
	    {
	        byte[] iv = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	        IvParameterSpec ivspec = new IvParameterSpec(iv);
	         
	        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
	        KeySpec spec = new PBEKeySpec(dechiper.toCharArray(), chiper.getBytes(), 65536, 256);
	        SecretKey tmp = factory.generateSecret(spec);
	        SecretKeySpec secretKey1 = new SecretKeySpec(tmp.getEncoded(), "AES");
	         
	        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
	        cipher.init(Cipher.DECRYPT_MODE, secretKey1, ivspec);
	       
	        FileInputStream inputStream = new FileInputStream(inputFile);
		       byte[] inputBytes = new byte[(int) inputFile.length()];
		       inputStream.read(inputBytes);

		       byte[] outputBytes = cipher.doFinal(inputBytes);
		       

		       String s = new String(outputBytes);
		      
		       inputStream.close();

		      return s;
	        
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
	        System.out.println("Error while decrypting: " + e.toString());
	    }
		return "";
	}
	public String convertDate(String dt) {
		Date date;
		try {
			date = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(dt);
			String dateString2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
			return dateString2;
		} catch (ParseException e) {
			System.out.println(e);
			return "-";
		}
		
		
	}
	
	public int compareDate(String endDate) {
		try {
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();  
		    String date3 = format.format(date);
			Date date1 = format.parse(endDate);
			Date date2 = format.parse(date3);
			
			if (date2.compareTo(date1) <= 0) {
			    return 1;
			}
		}catch(Exception e) {
			return 0;
		}
		return 0;	
	}
}
