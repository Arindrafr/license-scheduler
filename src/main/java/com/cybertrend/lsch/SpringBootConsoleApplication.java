package com.cybertrend.lsch;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@ComponentScan("com.cybertrend.lsch")
@EnableScheduling
public class SpringBootConsoleApplication  extends SpringBootServletInitializer {
    @Autowired
    DataSource dataSource;
    
	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringBootConsoleApplication.class);
    	builder.headless(false).run(args);
//        SpringApplication.run(SpringBootConsoleApplication.class, args);
    }
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootConsoleApplication.class);
    }
}